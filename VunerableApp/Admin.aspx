﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="VulnerableApp.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblAdmin" runat="server" Font-Size="Large"></asp:Label>
            <asp:Button ID="btnLogout" runat="server" Text="Logout" OnClick="btnLogout_Click" />
            <br />
            <br />
            <br />
            <asp:GridView ID="grdUsers" DataSourceID="sdsUsers" runat="server" DataKeyNames="Username" AutoGenerateColumns="false">
                <Columns>
                    <asp:CommandField ButtonType="Image" DeleteImageUrl="Images/Delete.bmp" ControlStyle-Width="20px" ShowDeleteButton="true" />
                    <asp:BoundField DataField="UserID" HeaderText="UserID" />
                    <asp:BoundField DataField="Username" HeaderText="Username" />
                    <asp:BoundField DataField="Password" HeaderText="Password" />
                    <asp:BoundField DataField="Role" HeaderText="Rolle" />
                </Columns>
            </asp:GridView>
            <asp:SqldataSource ID="sdsUsers" ConnectionString="Data Source=localhost\sqlexpress;Initial Catalog=VunerableApp;Integrated Security=True" SelectCommand="Select * from VunerableApp.dbo.Users" runat="server" DeleteCommand="Delete from VunerableApp.dbo.Users where Username = @Username"></asp:SqldataSource>
            <br />
            <h2>New User</h2>
            <asp:Label ID="lblNewUsername" Text="Username" runat="server"></asp:Label>
            <br />
            <asp:TextBox ID="txtNewUsername" runat="server"></asp:TextBox>
            <asp:Label ID="lblRequiredUsername" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblNewPassword" runat="server" Text="Password"></asp:Label>
            <br />
            <asp:TextBox ID="txtNewPassword" runat="server"></asp:TextBox>
            <asp:Label ID="lblRequiredPassword" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblRole" Text="Wähle die Rolle" runat="server"></asp:Label>
            <asp:DropDownList ID="ddlRoleAdmin" runat="server">
                <asp:ListItem Text="User" Value="User" />
                <asp:ListItem Text="Mod" Value="Mod" />
                <asp:ListItem Text="Admin" Value="Admin" Enabled="true" />
            </asp:DropDownList>
            <asp:DropDownList ID="ddlRole" runat="server">
                <asp:ListItem Text="User" Value="User" />
                <asp:ListItem Text="Mod" Value="Mod" />
                <asp:ListItem Text="Admin" Value="Admin" Enabled="false" />
            </asp:DropDownList>
            <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" />
        </div>
    </form>
</body>
</html>
