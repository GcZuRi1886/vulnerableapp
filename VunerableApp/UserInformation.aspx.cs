﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace VulnerableApp
{
    public partial class UserInformation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(@"Data Source=localhost\sqlexpress;Initial Catalog=VunerableApp;Integrated Security=True");
                conn.Open();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "Select UserID from VunerableApp.dbo.Users where Username = '" + Session["Username"] + "';";
                string UserID = (string)comm.ExecuteScalar().ToString();
                lblUserIDplaceholder.Text = UserID;

                lblUsernameplaceholder.Text = (string)Session["Username"];

                SqlCommand commpw = new SqlCommand();
                commpw.Connection = conn;
                commpw.CommandText = "Select Password from VunerableApp.dbo.Users where Username = '" + Session["Username"] + "';";
                string Password = (string)commpw.ExecuteScalar().ToString();
                lblPasswordplaceholder.Text = Password;

                SqlCommand commrole = new SqlCommand();
                commrole.Connection = conn;
                commrole.CommandText = "Select Role from VunerableApp.dbo.Users where Username = '" + Session["Username"] + "';";
                string Role = (string)commrole.ExecuteScalar().ToString();
                lblRoleplaceholder.Text = Role;
            }
            catch
            {

            }
            
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Done.aspx");
        }
        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["Username"] = null;
            Response.Redirect("Login.aspx");
        }
    }
}