﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Windows;

namespace VulnerableApp
{
    public partial class Admin : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection(@"Data Source=localhost\sqlexpress;Initial Catalog=VunerableApp;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                //MessageBox.Show("Du musst angemeldet sein um diese Aktion durchfüren zu können");
                Response.Redirect("Login.aspx");
            }
            else
            {
                conn.Open();
                SqlCommand validation = new SqlCommand();
                validation.Connection = conn;
                validation.CommandText = "Select Role From Users Where Username ='" + Session["Username"] + "';";
                string rank = (string)validation.ExecuteScalar();

                if (rank.Trim() == "Admin" || rank.Trim() == "Moderator")
                {
                    lblAdmin.Text = "Willkommen im Adminbereich " + (string)Session["Username"];
                }
                else
                {
                    MessageBox.Show("Dein Rang ist zu Niedrig " + rank);
                    Response.Redirect("Done.aspx");
                }
                if (rank.Trim() == "Admin")
                {
                    ddlRole.Visible = false;
                    ddlRoleAdmin.Visible = true;
                }
                else
                {
                    ddlRole.Visible = true;
                    ddlRoleAdmin.Visible = false;
                }
                conn.Close();
            }
        }
        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["Username"] = null;
            Response.Redirect("Login.aspx");
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand sqlUserID = new SqlCommand();
            sqlUserID.Connection = conn;
            sqlUserID.CommandText = "Select TOP(1) UserID From VunerableApp.dbo.Users Order by UserID DESC";
            string strUID = sqlUserID.ExecuteScalar().ToString();
            int UserID = Int32.Parse(strUID) + 1;
            SqlCommand query = new SqlCommand();
            query.Connection = conn;
            if (txtNewUsername.Text.Trim() == null && txtNewPassword.Text.Trim() == null)
            {
                MessageBox.Show(txtNewUsername.Text.Trim());
                /*if (ddlRoleAdmin.SelectedValue == "Admin")
                {
                    //query.CommandText = "Insert Into VunerabeApp.dbo.Users(UserID, Username, Password, Role) Values( '" + UserID +", "+ txtNewUsername.Text.Trim() + ", " + txtNewPassword.Text.Trim() + "', Admin);";
                    //MessageBox.Show(UserID + txtNewUsername.Text.Trim() + txtNewPassword.Text.Trim());
                }
                else if (ddlRole.SelectedValue == "User" || ddlRoleAdmin.SelectedValue == "User")
                {
                    //query.CommandText = "Insert Into VunerabeApp.dbo.Users(UserID, Username, Password, Role) Values( '" + UserID +", "+ txtNewUsername.Text.Trim() + ", " + txtNewPassword.Text.Trim() + "', User);";
                    //MessageBox.Show(UserID + txtNewUsername.Text.Trim() + txtNewPassword.Text.Trim());
                }
                else if (ddlRole.SelectedValue == "Mod" || ddlRoleAdmin.SelectedValue == "Mod")
                {
                    query.CommandText = "Insert Into VunerableApp.dbo.Users(UserID, Username, Password, Role) Values ( @UserID, @Username, @Password, Moderator);";
                }
                //query.CommandText = "Insert Into VunerableApp.dbo.Users(UserID, Username, Password, Role) Values ( @UserID, @Username, @Password, @Role);";
                query.Parameters.AddWithValue("@UserID", UserID);
                query.Parameters.AddWithValue("@Username", txtNewUsername.Text);
                query.Parameters.AddWithValue("@Password", txtNewPassword.Text);
                query.ExecuteNonQuery();*/
                //query.Parameters.AddWithValue("@Role", ddlRole.SelectedValue);
            }
            else if(txtNewUsername.Text == null)
            {
                lblRequiredUsername.Text = "Username is Required";
                lblRequiredUsername.Visible = true;
            }
            else if(txtNewPassword.Text == null)
            {
                lblRequiredPassword.Text = "Password is Required";
            }
            conn.Close();
        }
    }
}