﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Windows;
using MySql.Data.MySqlClient;
using System.Text;

namespace VulnerableApp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //string conn = ConfigurationManager.ConnectionStrings["Users"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Visible = false;
            HttpContext httpContext = HttpContext.Current;

            string authHeader = httpContext.Request.Headers["Authorization"];
            try
            {
                if (authHeader != null && authHeader.StartsWith("Basic"))
                {
                    string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                    string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                    int seperatorIndex = usernamePassword.IndexOf(':');

                    var username = usernamePassword.Substring(0, seperatorIndex);
                    var password = usernamePassword.Substring(seperatorIndex + 1);

                    SqlConnection conn = new SqlConnection(@"Data Source=localhost\sqlexpress;Initial Catalog=VunerableApp;Integrated Security=True");
                    SqlCommand query = new SqlCommand();
                    query.Connection = conn;
                    query.CommandText = "Select Username from VunerableApp.dbo.Users where Username = '" + username + "' and Password = '" + password + "';";
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(query);
                    adapter.Fill(dataTable);

                    if (dataTable.Rows.Count == 1)
                    {
                        lblError.Visible = false;
                        Session["Username"] = username;
                        Response.Redirect("./Done.aspx");
                    }
                    else
                    {
                        lblError.Text = "Invalid Credentials";
                        lblError.Visible = true;
                    }


                    Console.WriteLine(username + " " + password);
                    lblError.Visible = true;
                    lblError.Text = username + " " + password;
                }
                else
                {
                    //Handle what happens if that isn't the case
                    //throw new Exception("The authorization header is either empty or isn't Basic.");
                    lblError.Visible = true;
                    lblError.Text = "Header";
                }
            }
            catch
            {
                lblError.Visible = true;
                lblError.Text = "Header";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                /*MySqlConnection myconn = new MySqlConnection(@"Data Source=udefezuf.mysql.db.internal;Integrated Security=False;Initial Catalog=udefezuf_users.Users;User ID=udefezuf_fabian;password=hqRWtEeVPVtMvv9");
                MySqlCommand mycomm = new MySqlCommand();
                mycomm.Connection = myconn;
                mycomm.CommandText = "Select Username from udefezuf_users.Users where Username = '" + txtUsername.Text.Trim() + "' and Password = '" + txtPassword.Text.Trim() + "';";
                DataTable dataTable = new DataTable();
                MySqlDataAdapter myda = new MySqlDataAdapter(mycomm);
                myda.Fill(dataTable);*/
                SqlConnection conn = new SqlConnection(@"Data Source=localhost\sqlexpress;Initial Catalog=VunerableApp;Integrated Security=True");
                SqlCommand query = new SqlCommand();
                query.Connection = conn;
                query.CommandText = "Select Username from VunerableApp.dbo.Users where Username = '" + txtUsername.Text.Trim() + "' and Password = '" + txtPassword.Text.Trim() + "';";
                DataTable dataTable = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(query);
                adapter.Fill(dataTable);

                if (dataTable.Rows.Count == 1)
                {
                    lblError.Visible = false;
                    Session["Username"] = txtUsername.Text.Trim();
                    Response.Redirect("./Done.aspx");
                }
                else
                {
                    lblError.Text = "Invalid Credentials";
                    lblError.Visible = true;
                }
            }
            catch
            {
                lblError.Text = "Try again";
                lblError.Visible = true;
            }

        }
    }
}