﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VulnerableApp
{
    public partial class Done : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string[] keys = Request.Form.AllKeys;
                for (int i = 0; i < keys.Length; i++)
                {
                    Console.Write(keys[i] + ": " + Request.Form[keys[i]] + "<br>");
                }
                try
                {
                    SqlConnection conn = new SqlConnection(@"Data Source=localhost\sqlexpress;Initial Catalog=VunerableApp;Integrated Security=True");
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    command.CommandText = "Select Role From Users Where Username ='" + Session["Username"] + "';";
                    string rank = (string)command.ExecuteScalar();
                    if (rank.Trim() == "Admin")
                    {
                        lblUsername.Text = "Hallo " + (string)Session["Username"] + " mach was schönes mit deinen Adminrechten";
                        btnAdmin.Visible = true;
                    }
                    else if (rank.Trim() == "Moderator")
                    {
                        lblUsername.Text = "Hallo " + (string)Session["Username"] + " viel spass mit den Modrechten";
                        btnAdmin.Visible = true;
                    }
                    else
                    {
                        lblUsername.Text = "Hallo " + (string)Session["Username"] + " du bist ein " + rank;
                        btnAdmin.Visible = false;
                    }
                }
                catch
                {
                    Console.WriteLine("Error");
                }
            }
        }

        protected void btnAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("Admin.aspx");
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["Username"] = null;
            Response.Redirect("Login.aspx");
        }
        protected void btnUserInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserInformation.aspx");
        }
    }
}