﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInformation.aspx.cs" Inherits="VulnerableApp.UserInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblUserID" runat="server" Text="UserID: "></asp:Label>
            <asp:Label ID="lblUserIDplaceholder" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblUsername" runat="server" Text="Username: "></asp:Label>
            <asp:Label ID="lblUsernameplaceholder" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblPassword" runat="server" Text="Password: "></asp:Label>
            <asp:Label ID="lblPasswordplaceholder" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblRole" runat="server" Text="Role: "></asp:Label>
            <asp:Label ID="lblRoleplaceholder" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Button ID="btnBack" runat="server" Text="Zurück" OnClick="btnBack_Click" />
            <asp:Button ID="btnLogout" runat="server" OnClick="btnLogout_Click" Text="Logout" />
        </div>
    </form>
</body>
</html>
