﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Done.aspx.cs" Inherits="VulnerableApp.Done" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblUsername" runat="server"></asp:Label>
            <asp:Button ID="btnAdmin" runat="server" OnClick="btnAdmin_Click" Text="Adminbereich" />
            <asp:Button ID="btnUserInfo" runat="server" OnClick="btnUserInfo_Click" Text="User Info" />
            <asp:Button ID="btnLogout" runat="server" OnClick="btnLogout_Click" Text="Logout" />
        </div>
    </form>
</body>
</html>
